# nidhogg-ai

An AI for the game [nidhogg].
It uses [python-mss] to get the gamescreen and [pynput] to send keys.
(Hopefully) It'll teach itself how to play nidhogg using [reinforcement learning],
the same kind of machine learning AlphaGo Zero [used to become the worlds best
go player][alpha-go blog].

## The making

### IO

#### Sending keystrokes

So first of all, I needed a way to communicate with nidhogg. 
To send keys to active window (and also to switch windows using `ALT + TAB`),
I use pynput, maybe I'll even have an listener process in the future with which you can trigger certain events.
I tried to have nidhogg unfocused, but Windows makes it really hard to send keys to a window in the background,
as the window isn't expecting any input, so I trashed that idea.

So now I can send keys to nidhogg, but how does the program know which keys map to which move?
In earlier version, you needed to pass a dictionary to the `Player` class, which described that mapping,
but I realized later on, that the control files in `%APPDATA%` are relatively simple to parse.
And one evening later the `Player` class extracted its controls all by itself,
you only need to tell it which player it represents.

#### Grabbing the screen

Being able to make moves, it would be nice to see the effect of them,
so I need a way to grab the current game screen (multiple times a second).
To do that, I needed a high performance screen grabbing python library.
[Googling for exactly that][pynput google search] one finds [pynput].
Now I just always need to know where the nidhogg window is and my program can see it.
I just used [sizer] (as recommended [here][sizer recommendation]) to set the position.
Also I tried to make the window as small as possible keeping everything recognizealbe.
A smaller screen region is grabbed faster, resulting in ~60 FPS with a window size of 242x389.

#### Preparing the frames

To help our neural net a little bit with learning,
and also to be able to use the same net for both players, they need to be unified and prepared.
To make the frame look the same from both perspectives I game the current player a green and the opponent a red color.
I simply look for RBG values close to the player color and replace them.
After that I flip the image to the current player is always the left most player and the AI has to walk to the right.

To be able to see this prepared frame, I wanted a way to render frames to the in a seperate window.
For that task, [cv2] is great.
A basic loop to update the frame and it was done.
The only problem was, that while this loop was running, no other code was able to.

### Parallelisation

#### Threading

At first I thought, threading would be ideal for this, and I implemented it, and it worked... kinda.
Because of the GIL (global interpreter lock), only one python thread can use the interpreter at a time,
making multithreading pretty much pointless. But this being a common problem,
of course there where solutions: multiprocessing! Multithreading, but with processes instead of threads.

#### Multiprocessing

As each python process has its own interpreter, the GIL is not a problem anymore.
Luckily, the `multiprocessing` module is pretty similar to the `threading` module,
only a few changes here and there, and it worked.
So now, having all the IO done, being able to do multiple thing simultaneously, this is where the fun part begins...

### Logging

... almost! While coding the threading/multiprocessing stuff, I encountered a lot of errors.
Because of this, I started to use `print` statements in some places.
But as they started to get a little more complex, less DRY and annoying to comment out when I didn't need them,
I wanted to use the `logging` module, as it is the solution to all my problems (seriously, it was).


### Intelligence

So here the fun part begins!

#### The Autoencoder

An autoencoder is a neural net to compress and decompress data.
It's hour glass shaped and trained to reproduce the image it was given.

![Autoencoders are hour glass shaped][autoencoder image]

As it has to get all (or most of) the information through the dense spot in the middle,
it tries to make that information as useful as possible.
That representation in the middle is also called the _dense representation_ of the input data.
Watch [this video by carykh][carykh autoencoder] for a better explaination.

So, to train the magical black box which compresses data, one needs: data! ...lots of it, in fact!
I went to [youtube] and found a couple of videos without any watermark and in the right format, put them in [a playlist](https://www.youtube.com/playlist?list=PLMrjV7ZpwlD3WNiU-2dZvb452LT_0bWBG),
and let [yt-dl] download it. #####



[nidhogg]: http://nidhogggame.com/
[python-mss]: https://python-mss.readthedocs.io/index.html
[pynput]: https://pynput.readthedocs.io/en/latest/
[sizer]: http://www.brianapps.net/sizer/
[cv2]: https://pypi.org/project/opencv-python/
[yt-dl]: https://github.com/ytdl-org/youtube-dl

[reinforcement learning]: https://adventuresinmachinelearning.com/reinforcement-learning-tutorial-python-keras/
[alpha-go blog]: https://deepmind.com/blog/alphago-zero-learning-scratch/
[pynput google search]: https://www.google.com/search?q=high+performance+screen+grabbing+python+library
[sizer recommendation]: https://superuser.com/q/18215
[autoencoder image]: https://skymind.ai/images/wiki/deep_autoencoder.png
[carykh autoencoder]: https://www.youtube.com/watch?v=Sc7RiNgHHaE&t=7m41s