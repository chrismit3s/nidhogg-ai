import logging as log


log.basicConfig(format="{relativeCreated:>10.0f} - {levelname:<8} - {processName:<16} - {message}",  style="{", level=log.WARN)

GAME_POS   = (45, 45, 434, 288)
FRAME_SIZE = (GAME_POS[2] - GAME_POS[0], GAME_POS[3] - GAME_POS[1])


# imports from this module need to be done last as they may depend on some of the variables above
from src import autoencoder, files, helpers, nidhogg
