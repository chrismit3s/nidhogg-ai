from argparse import ArgumentParser
from src.autoencoder.dataset import create_arrays, extract_frames, prepare_frames
from src.helpers import vidshow


parser = ArgumentParser()
parser.add_argument("-q", "--quiet",
                    action="store_true",
                    help="surpress progress output")
parser.add_argument("--step",
                    type=int,
                    default=8,
                    help="the frame steps when extracting the frames")
parser.add_argument("--interpolation",
                    type=str,
                    default="lanczos4",
                    help="the interpolation when preparing the frames")
parser.add_argument("--blocks",
                    type=int,
                    default=16,
                    help="the number of blocks to split the data into")
args = parser.parse_args()


extract_frames(args.step, not args.quiet)
prepare_frames(args.interpolation, not args.quiet)
create_arrays(args.blocks, not args.quiet)
