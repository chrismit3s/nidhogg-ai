from numpy import asarray
from os import listdir, mkdir
from os.path import join
from src import FRAME_SIZE
from src.files import arrays, prepared_frames, extracted_frames, gameplay_video
from src.helpers import vidread, vidshow, get_num_frames, Loading
from src.nidhogg.gamehandle import GameHandle
from time import time
import cv2
import numpy as np


def extract_frames(step, verbose):
    # check if folder already exists
    try:
        mkdir(extracted_frames.path)
    except FileExistsError as e:
        raise FileExistsError("Cannot overwrite existing dataset")

    with Loading("Extracting frames", get_num_frames(gameplay_video), verbose) as l:
        for i, frame in enumerate(vidread(gameplay_video)):
            l.tick()
            if i % step == 0:
                cv2.imwrite(extracted_frames.next_name(), frame)
    return


def prepare_frames(interpolation, verbose):
    def _frame_gen():
        for filename in extracted_frames.iter_names():
            yield cv2.resize(cv2.imread(filename),
                             dsize=FRAME_SIZE,
                             interpolation=interpolation)
        pass

    # check if folder already exists
    try:
        mkdir(prepared_frames.path)
    except FileExistsError as e:
        raise FileExistsError("Cannot overwrite existing dataset")

    # look up interpolation if its a string
    if isinstance(interpolation, str):
        interpolation = getattr(cv2, "INTER_" + interpolation.upper())

    gh = GameHandle((0, 0, 0, 0))
    with Loading("preparing frames", extracted_frames.num_files() * 2, verbose) as l:
        for p in range(2):
            for frame in gh.prep_frame(player=p, frame_gen=_frame_gen()):
                l.tick()
                cv2.imwrite(prepared_frames.next_name(), frame)
    return
        

def create_arrays(blocks, verbose):
    # check if folder already exists
    try:
        mkdir(arrays.path)
    except FileExistsError as e:
        raise FileExistsError("Cannot overwrite existing dataset")

    # load the frames
    data = []
    with Loading("loading frames", prepared_frames.num_files(), verbose) as l:
        for i, filename in enumerate(prepared_frames.iter_names()):
            l.tick()
            data.append(cv2.imread(filename))

    # save them
    with Loading("saving blocks", blocks, verbose) as l:
        for i, block in enumerate(data):
            l.tick()
            np.save(arrays.full_name(i), np.asarray(block))
    return


def load_data(block=0):
    return np.load(arrays.full_name(block))
