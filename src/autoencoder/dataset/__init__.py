from src.autoencoder.dataset.convert import (
    extract_frames,
    prepare_frames,
    create_arrays,
    load_data)

DATA_FORMAT = "channels_last"

