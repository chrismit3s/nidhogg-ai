from keras.layers import Conv2D, Cropping2D, Dense, Flatten, Input, MaxPool2D, Reshape, UpSampling2D
from keras.models import Model
from numpy import prod, asarray
from src import FRAME_SIZE
from src.autoencoder.dataset import DATA_FORMAT
import numpy as np


def _add_channels(height, width, channels):
    return ((height, width, channels)
            if DATA_FORMAT == "channels_last"
            else (channels, height, width))


def _get_channels(data):
    return (data[-1]
            if DATA_FORMAT == "channels_last"
            else data[0])


def _remove_channels(data):
    return (data[:-1]
            if DATA_FORMAT == "channels_last"
            else data[1:])


def _replace_channels(data, channels):
    return _add_channels(*_remove_channels(data), channels)


def split_autoencoder(model):
    num_layers = len(model.layers)
    split_layer = min(range(num_layers), key=lambda i: prod(model.layers[i].output_shape[1:])) + 1
    i = 1

    # encoder
    x = encoder_in = Input(shape=model.layers[0].input_shape[1:])
    while i < split_layer:
        x = model.layers[i](x)
        i += 1
    encoder = Model(encoder_in, x)

    # decoder
    x = decoder_in = Input(shape=model.layers[i].input_shape[1:])
    while i < num_layers:
        x = model.layers[i](x)
        i += 1
    decoder = Model(decoder_in, x)

    return encoder, decoder
        


def parse_desc(in_list):
    out_list = []
    for cmd in in_list:
        # remove comments
        cmd = cmd.split("#")[0].lower().strip()

        # extract cmd and arg
        cmd, *arg = cmd.split()

        # convert arg to tuple/int/None
        if len(arg) == 0:  # no args
            arg = None
        elif len(arg) == 1:  # one arg
            if "x" in arg[0]:
                arg = tuple(int(n) for n in arg[0].split("x"))
            else:
                arg = int(arg[0])
        else:  # too many args
            raise ValueError(f"Too many args in line {cmd!r}")
        out_list.append((cmd, arg))
    return out_list


def calculate_shape(in_list):
    out_list = [("in",
                 None,
                 _add_channels(*FRAME_SIZE, 3))]  # input layer
    for cmd, arg in in_list:
        in_shape = out_list[-1][2]
        if cmd == "conv":
            out_list.append((cmd,
                             arg,
                             _replace_channels(in_shape, arg)))
        elif cmd == "scale":
            if isinstance(arg, int):
                arg = (arg, arg)
            out_list.append(("scale-d",
                             arg, 
                             np.ceil(asarray(in_shape) /
                                     _add_channels(*arg, 1)).astype("int")))
        elif cmd == "dense":
            # exception for flattening
            if len(in_shape) > 1:
                out_list.append(("flat",
                                 None, 
                                 (prod(in_shape),)))
            out_list.append(("dense",
                             arg, 
                             (arg,)))
    return out_list


def reverse_append(in_list):
    #    in            -> 200, 300, 3
    # -> conv 18       -> 200, 300, 18
    # -> scale-d 2x3   -> 100, 100, 18
    # -> flat          -> 180_000
    # -> dense 5       -> 5
    # -> dense 180_000 -> 180_000
    # -> reshape       -> 100, 100, 18
    # -> scale-u 2x3   -> 200, 300, 18
    # -> conv 18       -> 200, 300, 3
    # layers are mirror top to bottom;
    # sizes  also,  but instead of the
    # output  shape,  one needs to use
    # the input shape (output shape of
    # previous layer)

    # copy list and add anchors [elem0, anchor, elem1, ..., anchor]
    out_list = ([in_list[0], ("$", None, None)]
              + in_list[1:]
              + [("$", None, None)])
    for i, (cmd, arg, in_shape) in enumerate(in_list[:0:-1]):  # skip first in list (input layer) and go backwards
        out_shape = in_list[-(2 + i)][2]
        if cmd == "conv":
            out_list.append(("conv",
                             _get_channels(out_shape),
                             out_shape))
        elif cmd == "scale-d":
            out_list.append(("scale-u",
                             arg, 
                             out_shape))
        elif cmd == "flat":
            out_list.append(("reshape",
                             None, 
                             out_shape))
        elif cmd == "dense":
            out_list.append(("dense",
                             out_shape[0],
                             out_shape))
    out_list += [("$", None, None)]
    return out_list


def create_model(in_list):
    def UpCropping2D(dsize, target_shape, data_format, interpolation="nearest"):
        def _UpCropping2D(prev_layer):
            x = UpSampling2D(size=dsize, data_format=data_format, interpolation=interpolation)(prev_layer)
            out_shape = asarray(_remove_channels(x.shape[1:]), dtype="int")  # remove other dimensions (batch and channels dim)
            diff = out_shape - target_shape
            cropping = ((0, diff[0]), (0, diff[1]))
            return Cropping2D(cropping=cropping)(x)
        return _UpCropping2D

    anchors = []  # will contain input layer, dense representation, output layer
    x = None
    for i, (cmd, arg, out_shape) in enumerate(in_list):
        is_last_layer = (len(in_list) - 1 == i)
        if cmd == "$":  # anchor found
            anchors.append(x)
        elif cmd == "in":
            x = Input(shape=out_shape)
        elif cmd == "conv":
            x = Conv2D(filters=arg, kernel_size=(5, 5), data_format=DATA_FORMAT, padding="same", activation="relu" if is_last_layer else "sigmoid")(x)
        elif cmd == "dense":
            x = Dense(units=arg, activation="relu" if is_last_layer else "sigmoid")(x)
        elif cmd == "flat":
            x = Flatten()(x)
        elif cmd == "reshape":
            x = Reshape(target_shape=out_shape)(x)
        elif cmd == "scale-d":
            x = MaxPool2D(pool_size=arg, data_format=DATA_FORMAT, padding="same")(x)
        elif cmd == "scale-u":
            x = UpCropping2D(dsize=arg, target_shape=_remove_channels(out_shape), data_format=DATA_FORMAT)(x)
    return tuple(anchors)


def get_autoencoder_from_desc(desc):
    desc = parse_desc(desc)
    desc = calculate_shape(desc)
    desc = reverse_append(desc)
    in_layer, dense_layer, out_layer = create_model(desc)
    return Model(in_layer, out_layer)
