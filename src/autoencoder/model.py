from keras.callbacks import TensorBoard
from keras.models import load_model
from os import listdir
from src.autoencoder.build import get_autoencoder_from_desc, split_autoencoder
from src.autoencoder.dataset import DATA_FORMAT, load_data
from src.files import autoencoder_models, models_overview, arrays, logs
from src.helpers import Doing, Loading
from time import strftime, time


class AutoEncoder():
    def __init__(self, model_data=None):
        # from model number to file name
        if isinstance(model_data, int):
            model_data = autoencoder_models.full_name(model_data)

        # from path to desc file to desc list
        if isinstance(model_data, str) and model_data.endswith(".txt"):
           model_data = list(line[:-1] for line in open(model_data).readlines())  # remove newlines

        # get the model
        if isinstance(model_data, str):  # load model from file
            self.load_model(model_data)
        elif isinstance(model_data, list):  # create new model from desc list
            self.create_model(model_data)
        elif model_data is None:  # no model specified
            self.desc = self.model = None

    def create_model(self, desc):
        self.desc = desc
        self.model = get_autoencoder_from_desc(desc)
        self.encoder, self.decoder = split_autoencoder(self.model)

    def load_model(self, path):
        self.desc = None
        self.model = load_model(path)
        self.encoder, self.decoder = split_autoencoder(self.model)

    def compile(self, loss="mse", optimizer="adam"):
        if self.model is not None:
            self.model.compile(loss=loss, optimizer=optimizer)
            self.encoder.compile(loss=loss, optimizer=optimizer)
            self.decoder.compile(loss=loss, optimizer=optimizer)
        else:
            raise ValueError("No model set")

    def train(self, batch_size, epochs=30, verbose=True, dryrun=False):
        # load last data array as testing data
        with Doing("loading testing data"):
            x_test = load_data(-1)
        with Doing("preparing testing data"):
            x_test = x_test.astype("float32") / 255

        # iterate over all data arrays and train
        with Loading("training model", arrays.num_files() - 1) as l:
            for i in range(arrays.num_files() - 1):
                # those can be remove once fit callback is in place
                with Doing("loading training data"):
                    x_train = load_data(i)
                with Doing("preparing training data"):
                    x_train = x_train.astype("float32") / 255
                if dryrun:
                    print("=== TRAINING RIGHT NOW ===")
                    print(f"batch_size={batch_size}; epochs={epochs}")
                else:
                    self.model.fit(x_train, x_train,
                                   batch_size=batch_size,
                                   epochs=epochs,
                                   shuffle=True,
                                   verbose=1 if verbose else 0,
                                   validation_data=(x_test, x_test),
                                   callbacks=[TensorBoard(log_dir=logs)])
                del x_train
                l.tick()

    def save(self):
        if self.model is not None:
            with Doing("saving model"):
                filename = autoencoder_models.full_name().format(autoencoder_models.next_file_number())
                with open(models_overview, mode="a+") as overview:
                    # write header
                    overview.write(f"{filename}  {strftime('%Y-%m-%dT%H-%M-%S')}")

                    # write description/architecture if available
                    if self.desc is not None:
                        overview.write("\n    ".join([""] + self.desc))
                        overview.write("\n\n")
                    else: 
                        overview.write("no architecture info available")
                self.model.save(filename)
        else:
            raise ValueError("No model set")
