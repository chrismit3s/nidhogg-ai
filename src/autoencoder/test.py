from src.autoencoder.model import AutoEncoder
from src.nidhogg.gamehandle import GameHandle
from src import GAME_POS
from src.files import autoencoder_models
from keras.models import Model, load_model
from keras.layers import Input
from src.autoencoder.build import get_autoencoder_from_desc, split_autoencoder
from src.autoencoder.dataset import load_data
import numpy as np
import cv2


# Test first model
print("Loading model", end="", flush=True)
autoencoder = AutoEncoder(0)
autoencoder.compile()
print(" - done")

print("Model summaries:")
print_fn = lambda *args, **kwargs: (print("    ", *args, **kwargs)
                                    if args[0].replace("_", "") != ""
                                    else None)
print("  Autoencoder")
autoencoder.model.summary(print_fn=print_fn)
print("  Encoder")
autoencoder.encoder.summary(print_fn=print_fn)
print("  Decoder")
autoencoder.decoder.summary(print_fn=print_fn)

if input("Use [t]raining data or [l]ive game feed? ").lower() == "t":
    def gen():
        x_train = np.array(list(load_data(0)) + list(load_data(1)))
        for frame in x_train:
            yield frame
else:
	gen = GameHandle(GAME_POS).prep_frame
for frame in gen():
    cv2.imshow("Original frame", frame)

    decoded = autoencoder.model.predict(cv2.cvtColor(frame, cv2.COLOR_RGB2RGBA)[np.newaxis, ...])
    cv2.imshow("Autoencoded frame",
               cv2.cvtColor(decoded.squeeze(),
                            cv2.COLOR_RGBA2RGB))  # RGB to RGBA is the same as BGR TO BGRA

    encoded = autoencoder.encoder.predict(cv2.cvtColor(frame, cv2.COLOR_RGB2RGBA)[np.newaxis, ...])
    cv2.imshow("Encoded frame", cv2.resize(encoded, (0, 0), fx=10, fy=100))

    key = cv2.waitKey(1)
    if key == 27 or key == ord("q"):
        cv2.destroyAllWindows()
        break
