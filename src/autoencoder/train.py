from argparse import ArgumentParser
from src.autoencoder.model import AutoEncoder
from src.files import autoencoder_models
from os.path import join


parser = ArgumentParser()
parser.add_argument("-q", "--quiet",
                    action="store_true",
                    help="surpress progress output")
parser.add_argument("--loss",
                    type=str,
                    default="mse",
                    help="the loss function used in the model")
parser.add_argument("--optimizer",
                    type=str,
                    default="adam",
                    help="the optimizer function used in the model")
parser.add_argument("--batch-size",
                    type=int,
                    default=8,
                    help="the batch size")
parser.add_argument("--epochs",
                    type=int,
                    default=30,
                    help="the number of epochs the model should be trained")
parser.add_argument("--dryrun",
                    action="store_true",
                    help="dont really train/save a model")
args = parser.parse_args()


autoencoder = AutoEncoder(join(autoencoder_models.path,
                               "default-architecture.txt"))
autoencoder.compile(loss=args.loss,
                    optimizer=args.optimizer)
print("MODEL SUMMARY")
autoencoder.model.summary(print_fn=lambda x, *args, **kwargs: None if set(list(x)) == {"_"} else print(" ", x, *args, **kwargs))
print("")
print("TRAINING")
autoencoder.train(batch_size=args.batch_size,
                  epochs=args.epochs,
                  verbose=not args.quiet,
                  dryrun=args.dryrun)
if not args.dryrun:
    autoencoder.save()