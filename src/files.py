from os.path import join, expandvars
from src.helpers import Filename


autoencoder_models = Filename(join("models", "autoencoder"),      "autoencoder-{:02}.h5")
arrays             = Filename(join("data",   "arrays"),           "array-{:02}.npy")
prepared_frames    = Filename(join("data",   "prepared-frames"),  "prepared-{:06}.jpg")
extracted_frames   = Filename(join("data",   "extracted-frames"), "extracted-{:06}.jpg")
gameplay_video     = join("data", "gameplay.mp4")
appdata            = join(expandvars("%APPDATA%"), "Roaming", "Nidhogg")
models_overview    = join("models", "overview.txt")
logs               = join("logs")


