from src.nidhogg.gamehandle import GameHandle


#game_handle = GameHandle(region=(400, 400, 389 + 400, 243 + 400),
game_handle = GameHandle(region=(45, 45, 434, 288),  # get to this pos with sizer
                        p1=(0, "human"),
                        p2=(1, "random"))

# start players if wanted
if input("Start players? (y/N) ").lower() == "y":
    # bring game in focus
    game_handle.focus_and_unpause(2)

    # start players
    print("Starting players", end="", flush=True)
    game_handle.start_player(player=0, subprocess=True)
    game_handle.start_player(player=1, subprocess=True)
    print(" - done")

# show gamescreen
game_handle.render(subprocess=False,
                   show_fps=True,
                   zoom=3,
                   frame_gen=game_handle.prep_frame(0))
                   #frame_gen=game_handle.get_frame())
