from random import choice, random
from os.path import join
from src.helpers import get_key, win_reward
from src.files import appdata
import logging as log


color_margin = 30
colors = {"yellow": (  0, 255, 255),
          "orange": ( 64, 160, 255),
          "lime":   (  0, 244, 178),
          "green":  (150, 255,   0),
          "cyan":   (244, 212,   0),
          "red":    ( 71,  21, 255),
          "purple": (255, 102, 253),
          "blue":   (255, 102, 102)}
for key, val in colors.items():
    new_val = []
    for x in val:
        if x == 0:
            new_val.append(x + color_margin)
        elif x == 255:
            new_val.append(x - color_margin)
        else:
            new_val.append(x)
    colors[key] = new_val



def get_controls(appdata, player):
    def rotate_left(x, n):
        return ((x << n) | (x >> (16 - n))) & ((1 << 16) - 1)

    # all controls
    keywords = ["left", "right", "up", "down", "attack", "jump", "start", "back"]
    filepath = join(appdata, f"controlsPlayer{player + 1}.txt")
    try:
        with open(filepath) as file:
            # first line is keyboard controls, last character is a newline
            line = file.readline()[:-1]
    
            # convert line to byte list to string
            bytes = [int(line[x:x + 2], 16) for x in range(0, len(line), 2)]
            string = "".join(chr(x) for x in bytes)

            # extract for all keywords
            controls = {}
            for kw in keywords:
                index = string.find(kw) + len(kw) + 9  # 9 is a fixed offset
                result = rotate_left((bytes[index] << 8) + bytes[index + 1], 2)  # 2 bytes left is a fixed rotation

                # extract and add character
                controls[kw] = chr(result % 256)
    except FileNotFoundError:
        controls = {}
    return controls


def get_color(appdata, player):
    # find line
    try:
        file = open(join(appdata, "misc.ini"))
    except FileNotFoundError:
        color = ""
    else:
        for line in file:
            if line.startswith(f"p{player + 1}color="):
                break
        
        # extract part in quotes
        color = line.split('"')[1]

    if color.startswith("#"):  # hex string
        color = tuple(int(color[i:i + 2], 16) for i in range(1, 7, 2))
    elif color in colors:  # color name
        color = colors[color]
    else:  # no color passed -> default
        color = colors["yellow"] if player == 0 else colors["orange"]

    return color


class Player():
    """ storage  object for nidhogg  players,
    holds information about color,  controls,
    etc; also contains  decision  making code
    to generate moves 

    color: (str or tuple)
        the  color  of the  player,  either a
        name of a  default  color or a  tuple
        for the RGB value

    player: (int)
        which  player   in the game should be
        controled;   only  really  needed  to
        extract the controls

    type: (str)
        the  type  of  the   player;   either
        "human", "bot" or "{mode}",  where
        mode  is the way the ai  decides  its
        next move
    """

    def __init__(self,
                 player=0,
                 type="human",
                 color=None):
        log.log(5, f"called __init__({', '.join([key+'='+repr(val) for key, val in locals().items() if key != 'self'])})")

        # set player attributes
        self.player = player
        self.type = type

        # get color in bgra
        if color is None:  # get color
            self.color = get_color(appdata, self.player)
            log.info(f"extracted color from '{appdata}'")
        elif isinstance(color, str):  # color passed
            self.color = colors[color]

        # extract controls
        self.controls = {k: get_key(v) for k, v in get_controls(appdata, self.player).items()}
        log.info(f"extracted controls from '{appdata}'")
        
        # is player on the go
        self.go = None

    def get_move(self, frame_gen=None):
        """ returns  a  generator  object for the
        moves of the player

        frame_gen:
            frame   generator  used  to  generate
            frames
        """
        def random_move(self):
            while True:
                yield ([choice(list(self.controls))],)

        """ this is complete trash as you cant focus the terminal
        def move_terminal(self):
            move = "jump"
            duration = 0.1
            while True:
                # command is of shape: <move1>[,<move2,...] [duration]
                i = input(f"p{self.player + 1}-move? ").split(" ")

                if len(i) != 0:
                    moves = i[0].split(",")
                    duration = i[1] if len(i) > 1 else 0.1

                for i, move in enumerate(moves):
                    moves[i] = {"throw":      ["up", "attack"],
                                "roll-left":  ["down", "left"],
                                "roll-right": ["down", "right"]}.get(move, move)

                yield (moves, duration) """

        # check player type
        if self.type in ["human", "bot"]:
            err = f"players of type {self.type} can't make automated inputs"
            log.error(err)
            raise ValueError(err)

        return {"random":   random_move(self)}.get(self.type)
