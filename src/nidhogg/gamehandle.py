from functools import partial
from mss import mss
from multiprocessing import Process
from numpy import asarray
from pynput.keyboard import Controller
from src import GAME_POS, FRAME_SIZE
from src.helpers import get_key, replace_color, vidshow
from src.nidhogg.player import Player, color_margin
from time import time, sleep
from timeit import timeit
import cv2
import logging as log


player_color   = (51, 204, 51)
opponent_color = (51, 51, 204)


class _GetFrame():
    def __init__(self, region):
        self.region = region
        self.sct = mss()
        log.info("new mss instance created")

    def __iter__(self):
        return self
     
    def __next__(self):
        return asarray(self.sct.grab(self.region))[:, :, :-1]


class _PrepFrame():
    def __init__(self, frame_gen, player, original_color, margin, prepped_colors):
        self.frame_gen      = frame_gen
        self.player         = player
        self.original_color = asarray(original_color)
        self.margin         = margin
        self.prepped_colors = asarray(prepped_colors)

    def __iter__(self):
        return self
     
    def __next__(self):
        frame = next(self.frame_gen)

        # replace player with green and not player with red
        replace_color(frame, self.original_color[self.player],     self.margin, self.prepped_colors[0])
        replace_color(frame, self.original_color[1 - self.player], self.margin, self.prepped_colors[1])

        # flip frame (left-right) if player equals 1
        return frame[:, ::(1 - self.player * 2), :]


class GameHandle():
    """ a handle object for nidhogg,  manages
    in- and output

    region: (tuple)
        the region in which the nihogg window
        is  positioned,  boundaries of every-
        thing the players should see
    
    p1, p2:
        player object or tuple containing the
        properties  of  the  players   (type,
        color)
    """

    def __init__(self,
                 region,
                 p1=None,
                 p2=None):

        # initialize settings
        self.region = region
        self.processes = []  # process list (excluding the __main__ process)
        
        # initialize players
        self.prepped_colors = asarray([player_color, opponent_color])
        self.player1 = Player(*(p1 or (0, "human")))
        self.player2 = Player(*(p2 or (1, "bot")))
        self.players = [self.player1, self.player2]

        # set certain methods and attribute of players for easier and more intuitive use
        for i, p in enumerate(self.players):
            p.prep_frame = partial(self.prep_frame, i)
            p.make_moves = partial(self.make_moves, i)
            p.start      = partial(self.start_player, i)

        pass

    def _get_keyboard(self):
        """ makes   sure  a  keyboard  handle  is
        available """

        if not hasattr(self, "_keyboard"):
            self._keyboard = Controller#()
            log.info("created new keyboard handle")

        return self._keyboard

    def focus_and_unpause(self, n=1):
        """ switches  to  the  nth  window (which
        should be nidhogg) and unpauses it
        
        n:  which window to switch to
        """

        # make sure keyboard handle is a#vailable
        self._get_keyboard()
        
        # switch to game
        with self._keyboard.pressed(get_key("alt")):
            for i in range(n):
                self._keyboard.press(get_key("tab"))
                self._keyboard.release(get_key("tab"))
        sleep(0.05)

        # unpause game
        self._keyboard.press(get_key("enter"))
        sleep(0.05)
        self._keyboard.release(get_key("enter"))

        # delay until inputs can be processed
        sleep(0.3)

    def get_frame(self):
        """ yields a screenshot  of screen_region
        on  the  monitor;   takes  ~16.71 ms  per
        iteration """
        return _GetFrame(self.region)

    def prep_frame(self,
                   player=0,
                   frame_gen=None):
        """ prepares  each   frame   yielded   by
        frame_gen  so that the  player  specified
        goes from left  to  right  and  is  green
        while the  opponent  is red and yields it
        again; takes ~18.53 ms per iteration
        
        player: int
            perspective  from  which  player  the
            frame will be seen
            
        frame_gen:
            frame   generator  used  to  generate
            frames"""
        return _PrepFrame(frame_gen or self.get_frame(),
                          player,
                          [p.color for p in self.players],
                          color_margin,
                          self.prepped_colors)

    def render(self,
               title="python nidhogg game-handle",
               zoom=3,
               interpolation="nearest",
               show_fps=True,
               subprocess=False,
               frame_gen=None):
        """ render the game in a separate  window
        (and if wanted in a separate process)  as
        return by frame_gen
        
        title: str
            the title of the window

        zoom: int
            zoom factor

        interpolation: str
            interpolation used for zooming
            
        show_fps: bool
            whether or not to show fps in the top
            left corner of the render frame
            
        subprocess: bool
            whether or not to run the render in
            another process (makes this function
            non-blocking)
            
        frame_gen:
            frame   generator  used  to  generate
            frames """

        if subprocess:
            # remove keyboard handle if one exists (its not picklable)
            if hasattr(self, "_keyboard"):
                del self._keyboard
                log.info("removed keyboard handle")

            # create and start process
            self.processes.append(Process(target=self.render,
                                          name=f"RenderProcess-{len(self.processes)}",
                                          kwargs={"title": title + f" process #{len(self.processes)}",
                                                  "frame_gen": frame_gen,
                                                  "show_fps": show_fps,
                                                  "subprocess": False}))
            self.processes[-1].start()
            log.info(f"started process {self.processes[-1].name}")
            return None
        else:
            # look up interpolation if its a string
            if isinstance(interpolation, str):
                interpolation = getattr(cv2, "INTER_" + interpolation.upper())

            # if zoom is an int, make it a tuple
            if isinstance(zoom, int):
                zoom = (zoom, zoom)

            fps = 0
            t = time()
            n = 0

            frame_gen = frame_gen or self.get_frame()
            for frame in frame_gen:
                n += 1

                # zoom frame
                frame = cv2.resize(frame,
                                   dsize=(0, 0),
                                   fx=zoom[0],
                                   fy=zoom[1],
                                   interpolation=interpolation)

                # print and update fps
                if show_fps:
                    cv2.putText(frame, f"{fps:.0f} FPS", (0, 20), cv2.FONT_HERSHEY_PLAIN, 1.5, (255, 255, 255), 1, cv2.LINE_AA)
                if time() - t >= 1.0:
                    fps = n / (time() - t)
                    t = time()
                    n = 0
                    if fps <= 20.0:
                        log.warning(f"low fps ({fps:.0f}) detected")

                # update the window
                cv2.imshow(title, frame)

                # check for close
                key = cv2.waitKey(1)
                if key == 27 or key == ord("q") or cv2.getWindowProperty(title, 0) == -1:  # esc or q or window-x
                    cv2.destroyWindow(title)
                    break
        return

    def update_go(self):
        """ returns the player thats "on the go",
        returns None if no player has that status
        """
        
        frame = next(self.get_frame())
        r = None
        for i in range(2):
            if (self.players[i].color[0] <= frame[50, 330 - 300 * i]).all() and \
               (self.players[i].color[1] >= frame[50, 330 - 300 * i]).all():
                self.players[i].go = True
                r = i
            else:
                self.players[i].go = False
            log.debug(f"i = {i}; pixel = {tuple(frame[50, 30 + 300 * i])}; player.go = {self.players[i].go}")
        return r
    
    def make_moves(self,
                   player,
                   moves,
                   duration=-1):
        """ simulates  all  moves   specified  in
        moves  for  the player specified for dur-
        ation seconds
        
        
        player: int
            perspective  from  which  player  the
            frame will be seen
            
        moves: iterable
            iterable  containing  all moves to be
            made
            
        duration: int/float
            time the corresponding keys are to be
            pressed,  if its  negative,  the keys
            will be press until the next call"""

        # make sure keyboard handle is available
        self._get_keyboard()

        # release every key
        for key in self.players[player].controls.values():
            self._keyboard.release(key)

        # check if all keys are valid
        if len(moves) != len(set(moves) & set(self.players[player].controls)):
            err = f"move not found"
            log.error(err)
            log.debug(f"moves = {moves}")
            raise KeyError(err)

        # press all keys
        for move in moves:
            self._keyboard.press(self.players[player].controls[move])

        # sleep and release keys if wanted
        if duration > 0:
            sleep(duration)

            # release all keys
            for key in self.players[player].controls.values():
                self._keyboard.release(key)
        else:
            sleep(0.05)  # to limit max keypresses per second (DO NOT REMOVE)
        pass

    def start_player(self,
                     player,
                     subprocess=False):
        """ starts  making  moves  yielded by the
        players get_move function
        
        player: int
            player who will make the moves
            
        subprocess: bool
            whether  or  not  to  run  in another
            process  (makes  this  function  non-
            blocking)
        """

        if subprocess:
            # remove keyboard handle if one exists (its not picklable)
            if hasattr(self, "_keyboard"):
                del self._keyboard
                log.info("removed keyboard handle")

            # create and start process
            self.processes.append(Process(target=self.start_player,
                                          name=f"Player{player}Process-{len(self.processes)}",
                                          kwargs={"player": player,
                                                  "subprocess": False}))
            self.processes[-1].start()
            log.info(f"started process {self.processes[-1].name}")
            return None
        else:
            for args in self.players[player].get_move(frame_gen=self.prep_frame):
                self.players[player].make_moves(*args)
        pass


if __name__ == "__main__":
    results = []
    funcs = [x
             for x in dir(GameHandle)
             if "frame" in x]

    # simulation
    print("Simulating performance of frame generators")
    number = int(input("  number of simulations: "))
    for x in funcs:
        print(f"  simulating {x}", end="\r", flush=True)
        results.append((x, timeit(stmt=f"next(gh.{x}())",
                                  setup=f"from nidhogg import GameHandle; gh = GameHandle({GAME_POS})",
                                  number=number) / number * 1000))
        print(f"  simulating {x} - done")

    # present result
    print("")
    print("Results")
    gh = GameHandle(GAME_POS)
    for func, res in results:
        ret = next(getattr(gh, func)())
        print(f"  {str(func) + ':':<14} {res:6.3f} ms/iter => {1000 / res:4.1f} fps, returned {type(ret)} of shape {getattr(ret, 'shape', None)}")
