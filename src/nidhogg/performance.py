from src.nidhogg.gamehandle import GameHandle
from timeit import timeit
import cv2


def get_frame_gens():
    return [x
            for x in dir(GameHandle)
            if "frame" in x]


def get_interpolations():
    return [x[6:].lower()
            for x in dir(cv2)
            if x.startswith("INTER_")
            and x not in ["INTER_MAX", "INTER_TAB_SIZE", "INTER_TAB_SIZE2"]]


def verbose_iter(l):
    for x in l:
        print(f"Simulating {x}", end="", flush=True)
        yield x
        print(f" - done")


def time_per_iter(number, gen_str=""):
    return timeit(setup="from src.nidhogg.gamehandle import GameHandle; game_handle = GameHandle((45, 45, 434, 288))",
                  stmt="next(" + gen_str + ")",
                  number=number) / number


def simulate():
    funcs = get_frame_gens()

    # simulate
    results = []
    n = int(input("Number of iterations? (500) ") or 500)
    try:
        for func in verbose_iter(funcs):
            results.append(time_per_iter(n, f"game_handle.{func}()"))
    except KeyboardInterrupt:
        print(" - cancelled")

    # results
    print("Results:")
    max_len = max(len(x) for x in funcs) + 1
    for func, result in zip(funcs, results):
        print(f"  {(func + ':').rjust(max_len)} {result * 1000:6.2f}")
    

if __name__ == "__main__":
    simulate()
