from collections import namedtuple
from multiprocessing import Process
from numpy import asarray
from os import listdir
from os.path import join, exists
from pynput.keyboard import Key, KeyCode
from time import time
import cv2
import inspect
import logging as log
import numpy as np


class Doing():
    def __init__(self, text, verbose=True):
        self.text = text
        self.verbose = verbose
    
    def __enter__(self):
        self.t = time()
        self.update()
        return self
    
    def __exit__(self, exc_type, exc_value, traceback):
        if self.verbose:
            if exc_type is None:
                print(self.text + f" - done - took {time() - self.t:.0f}s")
            elif exc_type is KeyboardInterrupt:
                print(self.text + f" - aborted by user - after {time() - self.t:.0f}s")
                #return True  # prevents the exception from being propagated
            else:
                print(self.text + f" - aborted - after {time() - self.t:.0f}s")

    def update(self):
        if self.verbose:
            print(self.text, end="\r", flush=True)



class Loading(Doing):
    def __init__(self, text, max, verbose=True):
        super().__init__(text, verbose)
        self.max = max
        self.cur = 0

    def update(self):
        # f"{self.text} - {100 * self.cur / self.max:2.0f}% - {(time() - self.t) / self.cur * self.max:.0f}s remaining"
        if self.verbose:
            print(f"{self.text} - {100 * self.cur / self.max:.0f}%", end="\r", flush=True)

    def tick(self):
        self.cur += 1
        self.update()


class Filename(namedtuple("_Filename", ["path", "name"])):
    def full_name(self, n=None):
        # wrap indexes around (pythonic ;))
        if n is not None and n < 0:
            n += self.num_files()
        
        return join(self.path, self.name if n is None else self.name.format(n))

    def next_file_number(self):
        i = len(listdir(self.path))
        while not exists(self.full_name(i - 1)) and i > 0:
            i -= 1
        return i
    
    def next_name(self):
        return self.full_name(self.next_file_number())

    def num_files(self):
        return self.next_file_number()

    def iter_names(self):
        for i in range(self.num_files()):
            yield self.full_name().format(i)


def get_num_frames(filename):
    cap = cv2.VideoCapture(filename)
    return int(cap.get(cv2.CAP_PROP_FRAME_COUNT))


def vidread(filename):
    cap = cv2.VideoCapture(filename)

    # make sure stream is open
    if not cap.isOpened():
        cap.open()

    # ret turns false when stream is done
    ret = True
    while ret:
        ret, frame = cap.read()
        yield frame

    # close stream
    cap.release()


def vidshow(frame_gen, zoom=1, interpolation="nearest", title=None, delay=1):
    # look up interpolation if its a string
    if isinstance(interpolation, str):
        interpolation = getattr(cv2, "INTER_" + interpolation.upper())

    # if zoom is an int, make it a tuple
    if isinstance(zoom, int):
        zoom = (zoom, zoom)

    # use generator representation as default title
    title = title or f"{frame_gen!r}"
    for frame in frame_gen:
        # update the window
        cv2.imshow(title,
                   cv2.resize(frame,
                              dsize=(0, 0),
                              fx=zoom[0],
                              fy=zoom[1],
                              interpolation=interpolation))

        # check for close
        key = cv2.waitKey(delay)
        if key == 27 or key == ord("q") or cv2.getWindowProperty(title, 0) == -1:  # esc or q or window-x
            cv2.destroyWindow(title)
            break


def get_key(key):
    if key.lower() in dir(Key):
        return Key[key.lower()]
    else:
        return KeyCode(char=key)


def replace_color(img, target, margin, result):
    img[(np.absolute(target - img) <= margin).all(axis=2)] = result


def win_reward(t, s): 
    return (30 * s) / (t + 60) + 1 / 2


def reward_table(reward_func):
    times = list(range(0, 151, 30))
    r = []

    print("t\s |     win    lose")
    print("----+----------------")
    for time in times:
        r.append((reward_func(time, 1), reward_func(time, -1)))
        print(f"{time:3} | {r[-1][0]: 7.2f} {r[-1][1]: 7.2f}")

    if r[0][0] > r[1][0] and \
       r[1][0] > r[1][1] and \
       r[1][1] > r[0][1]:
        s = inspect.getsource(reward_func)
        s = s[s.find("return") + 7:-1]
        print("")
        print(f"r(t, s) = {s}  is a possible candidate")
