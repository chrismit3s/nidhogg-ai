# todo

* `train-new-model.bat` script which checks if the dataset is prepared and trains a new model

## nidhogg

* fix (rewrite!) render in subprocess
* subprocess thingy as decorator
  * no `get_keyboard` method, remove everything
    unpicklable before pickle and create
    everything again after `fork`/`spawn`
* **listener process to start events**
* split gamehandle into:
  * a handle object for soley game IO related things (like `tiles.py` the [2048 project](https://gitlab.com/chrismit3s/2048-ai))
  * a "user interface" (render, focus_and_unpause)

## autoencoder 

* rewrite constructor
* custom call back
* train on tensorpad
* PCA on results
* extract desc when loading from disk

### **dataset**

* [dataset generator](https://medium.com/@mrgarg.rajat/training-on-large-datasets-that-dont-fit-in-memory-in-keras-60a974785d71#2ad4)
  * redo files (no folders needed for most dataset related things, just name template)
  * in main, [create temp dir](https://docs.python.org/3/library/tempfile.html#examples) and pass name template for each step (in and out, full path and name with {})
* make file to train new model (`train.py`)

## q-learning

* [resource](https://adventuresinmachinelearning.com/reinforcement-learning-tutorial-python-keras/)
* action: one key of controls
* state: next frame
* rewards (reward for opponent -> punish): 
  * for win (win_reward in helpers)
  * for gaining(2x)/holding(1x) go
  * for pressing left (if on go)
  * for a transition in the right direction

## readme

* some notes for readme regarding the model build:
  * [training data](https://www.youtube.com/playlist?list=PLMrjV7ZpwlD3WNiU-2dZvb452LT_0bWBG)
  * output must be the same as input
  * input is 389x243x3 downsampling/pooling makes rounding errors
  * upsampling results in different size because of those errors
  * cropping!!
